package com.example.singhkshitiz.letschat;

import android.content.Context;
import android.content.Intent;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import de.hdodenhof.circleimageview.CircleImageView;

public class UserActivity extends AppCompatActivity {

    private RecyclerView mUsersList;
    private DatabaseReference mUsersDatabaseReference;
    private RecyclerView.Adapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        mUsersList=(RecyclerView)findViewById(R.id.recyclerViewUsersList);
        mUsersList.setHasFixedSize(true);
        mUsersList.setLayoutManager(new LinearLayoutManager(this));

        mUsersDatabaseReference= FirebaseDatabase.getInstance().getReference().child("users");
     //   mUsersDatabaseReference.keepSynced(true);

    }

    private void attachRecyclerViewAdapter() {
         adapter = newAdapter();

        // Scroll to bottom on new messages
        adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                mUsersList.smoothScrollToPosition(adapter.getItemCount());
            }
        });

        mUsersList.setAdapter(adapter);
    }



    protected RecyclerView.Adapter newAdapter() {

        FirebaseRecyclerOptions<Users> options =
                new FirebaseRecyclerOptions.Builder<Users>()
                        .setQuery(mUsersDatabaseReference, Users.class)
                        .setLifecycleOwner(this)
                        .build();
        //-------FIREBASE RECYCLE VIEW ADAPTER-------
//        FirebaseRecyclerAdapter<Users, UserViewHolder> firebaseRecyclerAdapter
//                = new FirebaseRecyclerAdapter<Users, UserViewHolder>(options)
        return new FirebaseRecyclerAdapter<Users, UserViewHolder>(options)

            {
                @Override
                public UserViewHolder onCreateViewHolder (ViewGroup parent,int viewType){
                return new UserViewHolder(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.recycle_list_single_user, parent, false));
            }

                @Override
                protected void onBindViewHolder(@NonNull @NotNull UserViewHolder viewHolder, int position,
                                                @NonNull @NotNull Users users) {

                    viewHolder.setName(users.getName());
                    viewHolder.setStatus(users.getStatus());
                    viewHolder.setImage(users.getThumbImage(),getApplicationContext());
                    final String user_id=getRef(position).getKey();

                    viewHolder.mView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            Intent profileIntent=new Intent(UserActivity.this,ProfileActivity.class);
                            profileIntent.putExtra("user_id",user_id);
                            startActivity(profileIntent);
                        }
                    });
            }

                @Override
                public void onDataChanged () {
                // If there are no chat messages, show a view that invites the user to add a message.
//                mEmptyListMessage.setVisibility(getItemCount() == 0 ? View.VISIBLE : View.GONE);
            }
        };
    }


    @Override
    protected void onStart() {
        super.onStart();
        attachRecyclerViewAdapter();

//        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
//        mUsersDatabaseReference.child(uid).child("online").setValue("true");
//        Query conversationQuery = mConvDatabase.orderByChild("time_stamp");

//        FirebaseRecyclerOptions<Users> options =
//                new FirebaseRecyclerOptions.Builder<Users>()
//                        .setQuery(mUsersDatabaseReference, Users.class)
//                        .build();
        //-------FIREBASE RECYCLE VIEW ADAPTER-------
//        FirebaseRecyclerAdapter<Users, UserViewHolder> firebaseRecyclerAdapter
//                = new FirebaseRecyclerAdapter<Users, UserViewHolder>(options)

//        FirebaseRecyclerAdapter<Users , UserViewHolder> firebaseRecyclerAdapter=new FirebaseRecyclerAdapter<Users, UserViewHolder>(
//                Users.class,
//                R.layout.recycle_list_single_user,
//                UserViewHolder.class,
//                mUsersDatabaseReference
//        ) {
//        {
//            @NonNull
//            @NotNull
//            @Override
//            public UserViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
//                View view = LayoutInflater.from(parent.getContext())
//                        .inflate(R.layout.recycle_list_single_user, parent, false);
//
//                return new UserViewHolder(view);
//            }
//
//            @Override
//            protected void onBindViewHolder(@NonNull @NotNull UserViewHolder viewHolder, int position, @NonNull @NotNull Users users) {
//
//
//                viewHolder.setName(users.getName());
//                viewHolder.setStatus(users.getStatus());
//                viewHolder.setImage(users.getThumbImage(),getApplicationContext());
//                final String user_id=getRef(position).getKey();
//
//                viewHolder.mView.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//
//                        Intent profileIntent=new Intent(UserActivity.this,ProfileActivity.class);
//                        profileIntent.putExtra("user_id",user_id);
//                        startActivity(profileIntent);
//                    }
//                });
//            }
//        };
      //  mUsersList.setAdapter(firebaseRecyclerAdapter);
    }

    public static class UserViewHolder extends RecyclerView.ViewHolder{
        View mView;
        public UserViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
        }

        public void setName(String name) {
            TextView userNameView=(TextView)mView.findViewById(R.id.textViewSingleListName);
            userNameView.setText(name);
        }


        public void setStatus(String status) {
            TextView userStatusView=(TextView)mView.findViewById(R.id.textViewSingleListStatus);
            userStatusView.setText(status);
        }

        public void setImage(String thumb_image,Context ctx) {
            CircleImageView userImageView = (CircleImageView)mView.findViewById(R.id.circleImageViewUserImage);
            //Log.e("thumb URL is--- ",thumb_image);
            Picasso.with(ctx).load(thumb_image).placeholder(R.drawable.user_img).into(userImageView);
        }
    }

    @Override
    protected void onStop() {
        //String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        //mUsersDatabaseReference.child(uid).child("online").setValue(ServerValue.TIMESTAMP);

        super.onStop();
    }
}
